package com.webserver.controller;

import com.webserver.annotation.Controller;
import com.webserver.annotation.RequestMapping;
import com.webserver.core.DispatcherServlet;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;
import qrcode.QRCodeUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Random;

@Controller
public class ToolsController {
    private static File imageDir;
    private static File root;
    private static File staticDir;

    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root, "static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        imageDir = new File("./images");
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
    }

    @RequestMapping("/myweb/QRUtilHtml")
    public void createQrHtml(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter pw = response.getWriter();

        pw.println("<!DOCTYPE html>");
        pw.println("<html lang=\"en\">");
        pw.println("<head>");
        pw.println("<meta charset=\"UTF-8\">");
        pw.println("<title>二维码生成工具</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<center>");
        pw.println("<h1>二维码生成工具</h1>");
        pw.println("<form action=\"/myweb/QRUtil\" method=\"post\">");
        pw.println("<table border=\"2\">");
        pw.println("<tr>");
        pw.println("<td>文本内容</td>");
        pw.println("<td><input type=\"text\" name=\"message\"></td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"生成\"></td>");
        pw.println("</tr>");
        pw.println("</table>");
        pw.println("</form>");
        pw.println("</center>");
        pw.println("</body>");
        pw.println("</html>");
        pw.println("");
        pw.println("");
        pw.println("");

        response.setContentType("text/html");


    }

    @RequestMapping("/myweb/QRUtil")
    public void createQr(HttpServletRequest request, HttpServletResponse response) {
        String content = request.getParameter("message");
        Random a = new Random();

        String name = "image" + String.valueOf(a.nextInt(10)) + ".png";
        try {
            QRCodeUtil.encode(content, "logo.jpg", new FileOutputStream(staticDir + "/myweb/" + name), true);

        } catch (Exception e) {
            e.printStackTrace();
        }


        PrintWriter pw = response.getWriter();


        pw.println("<!DOCTYPE html>");
        pw.println("<html lang=\"en\">");
        pw.println("<head>");
        pw.println("<meta charset=\"UTF-8\">");
        pw.println("<title>二维码生成工具</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<center>");
        pw.println("<h1>二维码生成工具</h1>");
        pw.println("<form action=\"/myweb/QRUtil\" method=\"post\">");
        pw.println("<table border=\"2\">");
        pw.println("<tr>");
        pw.println("<td>文本内容</td>");
        pw.println("<td><input type=\"text\" name=\"message\" value=\"" + content + "\"></td>");
        pw.println("</tr>");
        //-----------
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\">");
        pw.println("<img src=\"/myweb/" + name + "\">");
        pw.println("<td>");
        pw.println("</tr>");
        //-----------
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"生成\"></td>");
        pw.println("</tr>");
        pw.println("</table>");
        pw.println("</form>");
        pw.println("</center>");
        pw.println("</body>");
        pw.println("</html>");
        pw.println("");
        pw.println("");
        pw.println("");

        response.setContentType("text/html");
        File file = new File("./");
        file.delete();

    }

    @RequestMapping("/myweb/random.jpg")
    public void createRandomImage(HttpServletRequest request, HttpServletResponse response) {
        //1创建一张空图片，并且指定宽高。 理解为:创建一张画纸
        BufferedImage image = new BufferedImage(70, 30, BufferedImage.TYPE_INT_RGB);

        //2根据图片获取一个画笔，通过该画笔画的内容都会画到该图片上
        Graphics g = image.getGraphics();

        //3确定验证码内容(字母与数字的组合)
        String line = "abcdefghjiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();//用于生成随机数(随机数位line的字符下标)

        //为图片背景填充一个随机颜色
        //创建Color时，需要指定三个参数，分别是，红，绿，蓝。数字范围都是(0-255)之间
        Color bgcolor = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
        //将画笔设置为该颜色
        g.setColor(bgcolor);
        //填充整张图片为画笔当前颜色
        g.fillRect(0, 0, 70, 30);

        String code = "";
        //向图片上画4个字符
        for (int i = 0; i < 4; i++) {
            //随机生成一个字符
            String str = line.charAt(random.nextInt(line.length())) + "";
            //生成随机颜色
            Color color = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
            //设置画笔颜色
            g.setColor(color);
            //设置字体
            g.setFont(new Font(null, Font.BOLD, 20));
            //将字符串画到图片指定的位置上
            g.drawString(str, i * 15 + 5, 18 + random.nextInt(11) - 5);
            code = code + str;
        }

        //随机生成4条干扰线
        for (int i = 0; i < 4; i++) {
            Color color = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
            g.setColor(color);
            g.drawLine(random.nextInt(71), random.nextInt(31),
                    random.nextInt(71), random.nextInt(31));
        }

        //将图片写入文件来生成该图片文件
        try {
            ImageIO.write(image, "jpg", response.getOutputStream());
            response.setContentType("image/jpeg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
//        String message = "http://doc.canglaoshi.org";
//        try {
//            //参数1:二维码上包含的文本信息  参数2:图片生成的位置
////            QRCodeUtil.encode(message,"./qr.jpg");
//            //参数1:二维码上包含的文本信息  参数2:图片生成后会通过该流写出
////            QRCodeUtil.encode(message,new FileOutputStream("./qr.jpg"));
//            //参数1:二维码上包含的文本信息  参数2:二维码中间的logo图片 参数3:图片生成的位置 参数4:是否需要压缩logo图片到中间大小
////            QRCodeUtil.encode(message,"logo.jpg","./qr.jpg",true);
//
//            QRCodeUtil.encode(message,"logo.jpg",new FileOutputStream("./qr.jpg"),true);
//
//
//            System.out.println("二维码生成完毕!");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }
}
