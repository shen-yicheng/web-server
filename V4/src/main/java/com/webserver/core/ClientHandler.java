package com.webserver.core;


import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *  该线程任务负责与指定客户端完成HTTP交互
 *  与客户端交流的流程分成三步：
 *      1.解析请求
 *      2.处理请求
 *      3.发送响应
 *
 */
public class ClientHandler implements Runnable {
    private Socket socket;
    public  ClientHandler(Socket socket){
        this.socket = socket;
    }


    @Override
    public void run() {

        try {
            //1.解析请求
            //1.解析请求行
            String line = readLine();
            System.out.println(line);
            //请求行相关信息
            String method;      //请求方式
            String URI;         //抽象路径
            String protocol;    //协议版本

/*
            //方法1
            method = line.substring(0,line.indexOf(" "));
            URI = line.substring(line.indexOf(" ")+1,line.lastIndexOf(" "));
            protocol = line.substring(line.lastIndexOf(" ")+1,line.length());

            System.out.println(">>> 请求方式："+ method);            //GET
            System.out.println(">>> 抽象路径："+ URI);               // myweb/index.html
            System.out.println(">>> 协议版本："+ protocol);          // /HTTP/1.1
 */


            //方法2
            String[] get = line.split(" ");

            System.out.println(">>> 请求方式："+ get[0]);            //GET
            System.out.println(">>> 抽象路径："+ get[1]);            // myweb/index.html
            System.out.println(">>> 协议版本："+ get[2]);            // /HTTP/1.1

            //1.2解析消息头
            Map<String,String> headers = new HashMap<>();
            while (true){
                line = readLine();
                if (line.isEmpty()){
                    break;
                }
                System.out.println(">>> 消息头："+line);
                String[] header = line.split(": ");
                headers.put(header[0],header[1]);
            }
            Set<Map.Entry<String,String>> headers1 = headers.entrySet();
            for(Map.Entry<String,String> e : headers1){
                String key = e.getKey();
                String value = e.getValue();
                System.out.println(">>> "+key+"---"+value);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 通过socket获取的输入流读取客户端发送来的一行字符串
     * @return
     */
    private String readLine() throws IOException {
        InputStream in = socket.getInputStream();
        //读取一行先决条件：以连续的两个字符(回车符(13)和换行符(10))作为结束这一行的标志。
        char pre ='.',cur='.';                  //pre上次读取的字符，cur本次读取的字符
        StringBuilder builder = new StringBuilder();
        int d;
        while ((d = in.read()) != -1){
            cur = (char)d;                      //本次读取到的字符
            if (pre==13&&cur==10){              //判断是否连续读取到了回车和换行
                break;
            }
            builder.append(cur);
            pre = cur;                          //在进行下次读取字符前将本次读取的字符记作上次读取的字符
        }
        return builder.toString().trim();
    }
}
