package com.webserver.entity;

import java.io.Serializable;

public class Article implements Serializable {
    static final long serialVersionUID = 1L;
    private String title;
    private String massage;

    public Article(){}

    public Article(String title, String massage) {
        this.title = title;
        this.massage = massage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", massage='" + massage + '\'' +
                '}';
    }
}
