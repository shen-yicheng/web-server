package com.webserver.core;

import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.File;
import java.net.URISyntaxException;

public class DispatcherServlet {
    private static File root;
    private static File staticDir;
    static {
        try {
            root = new File(ClientHandler.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        //2 处理请求
        // http://localhost:8088/myweb/index.html
        //通过请求对象，获取浏览器地址栏中的抽象路径部分
        String path = request.getUri(); //path:/myweb/index.html

        File file =new File(staticDir,path);

        System.out.println("资源是否存在:"+file.exists());
        if (file.isFile()){
            response.setContentFile(file);
        }else{
            response.setStatusCode(404);
            response.setStatusReason("NotFound");
            file = new File(staticDir,"root/404.html");
            response.setContentFile(file);
        }
    }
}
