package com.webserver.controller;

import com.webserver.core.DispatcherServlet;
import com.webserver.entity.Article;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;

public class ArticleController {
    private static File articleDir;
    private static File root;
    private static File staticDir;
    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        articleDir = new File("./articles");
        if (!articleDir.exists()){
            articleDir.mkdirs();
        }
    }


    public void writeArticle(HttpServletRequest request, HttpServletResponse response){
        //1.获取用户在注册页面上输入的注册信息,获取form表单提交的内容
        String title = request.getParameter("title");
        String message = request.getParameter("message");

        System.out.println(title+","+message);

        //2.将用户信息保存
        File articlFile = new File(articleDir,title+".obj");
        try(
                FileOutputStream fos = new FileOutputStream(articlFile);
                ObjectOutputStream oos = new ObjectOutputStream(fos)
        ){
            Article article = new Article(title,message);
            oos.writeObject(article);

            //注册成功了
            File file = new File(staticDir,"/myweb/article_success.html");
            response.setContentFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //3.给用户响应一个注册结果(成功或者失败)

    }
}
