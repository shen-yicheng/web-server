package com.webserver.controller;

import com.webserver.core.DispatcherServlet;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;
import qrcode.QRCodeUtil;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Random;

public class ToolsController {
    private static File imageDir;
    private static File root;
    private static File staticDir;

    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root, "static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        imageDir = new File("./images");
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
    }

    public void createQrHtml(HttpServletRequest request, HttpServletResponse response){
        PrintWriter pw = response.getWriter();

        pw.println("<!DOCTYPE html>");
        pw.println("<html lang=\"en\">");
        pw.println("<head>");
        pw.println("<meta charset=\"UTF-8\">");
        pw.println("<title>二维码生成工具</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<center>");
        pw.println("<h1>二维码生成工具</h1>");
        pw.println("<form action=\"/myweb/QRUtil\" method=\"post\">");
        pw.println("<table border=\"2\">");
        pw.println("<tr>");
        pw.println("<td>文本内容</td>");
        pw.println("<td><input type=\"text\" name=\"message\"></td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"生成\"></td>");
        pw.println("</tr>");
        pw.println("</table>");
        pw.println("</form>");
        pw.println("</center>");
        pw.println("</body>");
        pw.println("</html>");
        pw.println("");
        pw.println("");
        pw.println("");

        response.setContentType("text/html");




    }
    public void createQr(HttpServletRequest request, HttpServletResponse response){
        String content = request.getParameter("message");
        Random a = new Random();

        String name = "image"+String.valueOf(a.nextInt(10))+".png";
        try {
            QRCodeUtil.encode(content,"logo.jpg",new FileOutputStream(staticDir+"/myweb/"+name),true);

        } catch (Exception e) {
            e.printStackTrace();
        }



        PrintWriter pw = response.getWriter();


        pw.println("<!DOCTYPE html>");
        pw.println("<html lang=\"en\">");
        pw.println("<head>");
        pw.println("<meta charset=\"UTF-8\">");
        pw.println("<title>二维码生成工具</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<center>");
        pw.println("<h1>二维码生成工具</h1>");
        pw.println("<form action=\"/myweb/QRUtil\" method=\"post\">");
        pw.println("<table border=\"2\">");
        pw.println("<tr>");
        pw.println("<td>文本内容</td>");
        pw.println("<td><input type=\"text\" name=\"message\" value=\""+content+"\"></td>");
        pw.println("</tr>");
        //-----------
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\">");
        pw.println("<img src=\"/myweb/"+name+"\">");
        pw.println("<td>");
        pw.println("</tr>");
        //-----------
        pw.println("<tr>");
        pw.println("<td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"生成\"></td>");
        pw.println("</tr>");
        pw.println("</table>");
        pw.println("</form>");
        pw.println("</center>");
        pw.println("</body>");
        pw.println("</html>");
        pw.println("");
        pw.println("");
        pw.println("");

        response.setContentType("text/html");
        File file = new File("./");
        file.delete();

    }

    public static void main(String[] args) {
        String message = "http://doc.canglaoshi.org";
        try {
            //参数1:二维码上包含的文本信息  参数2:图片生成的位置
//            QRCodeUtil.encode(message,"./qr.jpg");
            //参数1:二维码上包含的文本信息  参数2:图片生成后会通过该流写出
//            QRCodeUtil.encode(message,new FileOutputStream("./qr.jpg"));
            //参数1:二维码上包含的文本信息  参数2:二维码中间的logo图片 参数3:图片生成的位置 参数4:是否需要压缩logo图片到中间大小
//            QRCodeUtil.encode(message,"logo.jpg","./qr.jpg",true);

            QRCodeUtil.encode(message,"logo.jpg",new FileOutputStream("./qr.jpg"),true);


            System.out.println("二维码生成完毕!");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
