package com.webserver.controller;


import com.webserver.core.DispatcherServlet;
import com.webserver.entity.User;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理与用户相关的业务操作
 */
public class UserController {
    private static File userDir;
    private static File root;
    private static File staticDir;
    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        userDir = new File("./users");
        if (!userDir.exists()){
            userDir.mkdirs();
        }
    }


    /**
     * 用户注册
     * @param request
     * @param response
     */
    public void reg(HttpServletRequest request, HttpServletResponse response){
        //1 获取用户注册页面上输入的注册信息，获取form表单提交的内容
        /*
            getParameter传入的值必须和页面表单上对应输入框的名字一致
            即:<input name="username" type="text">
                            ^^^^^^^
                            以它一致
         */
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String nickname = request.getParameter("nickname");
        String ageStr = request.getParameter("age");

        /*
            必要的验证，要求:
            四项信息不能为null，并且年龄必须是一个数字(正则表达式)
            否则直接给用户一个注册失败的页面:reg_input_error.html
            该页面剧中显示一行字:输入信息有误，注册失败。
            实现思路:
            添加一个分支判断，如果符合了上述的情况，直接创建一个File对象表示
            错误提示页面，然后将其设置到响应对象的正文上即可。否则才执行下面
            原有的注册操作。
         */
        if(username==null||password==null||nickname==null||ageStr==null||
                !ageStr.matches("[0-9]+")){
            File file = new File(staticDir,"/myweb/reg_input_error.html");
            response.setContentFile(file);
            return;
        }
        int age = Integer.parseInt(ageStr);
        System.out.println(username+","+password+","+nickname+","+ageStr);


        //2 将用户信息保存
        File userFile = new File(userDir,username+".obj");
        /*
            判断是否为重复用户，若重复用户，则直接响应页面:have_user.html
            该页面剧中显示一行字:该用户已存在，请重新注册
         */
        if(userFile.exists()){//文件存在则说明是重复用户
            File file = new File(staticDir,"/myweb/have_user.html");
            response.setContentFile(file);
            return;
        }


        try (
                FileOutputStream fos = new FileOutputStream(userFile);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ){
            User user = new User(username,password,nickname,age);
            oos.writeObject(user);

            //注册成功了
            File file = new File(staticDir,"/myweb/reg_success.html");
            response.setContentFile(file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * 用户登录
     * @param request
     * @param response
     */
    public void login(HttpServletRequest request, HttpServletResponse response){


        //1.获取用户在登录页面上输入的登录信息,获取form表单提交的内容
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("登录："+username+","+password);
        if (username==null||password==null){
            File file = new File(staticDir,"/myweb/login_info_error.html");
            response.setContentFile(file);
            return;
        }
        //2.获取User文件夹下是否存在username.obj
        File userFile = new File(userDir,username+".obj");
        if (userFile.exists()){
            try {
                User a = (User) new ObjectInputStream(new FileInputStream(userFile)).readObject();
                String child = null;
                System.out.println(a.getPassword());
                if (password.equals(a.getPassword())){
                    response.setContentFile(new File(staticDir,"/myweb/login_success.html"));
                    return;
                }
            } catch (IOException|ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        response.setContentFile(new File(staticDir,"/myweb/login_info_error.html"));
    }

    /**
     * 用于显示用户列表的动态页面
     * @param request
     * @param response
     */
    public void showAllUser(HttpServletRequest request, HttpServletResponse response){
        //1.先用users目录里所偶的用户读取出来存入一个list集合备用
        List<User> userList = new ArrayList<>();
        /*
            首先获取users中所有的子项 提示userDir.listFiles
         */
        File[]subs = userDir.listFiles(f->f.getName().endsWith(".obj"));
        for (File f : subs){
            try {
                FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis);
                User user = (User) ois.readObject();
                userList.add(user);
            } catch (IOException|ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        //2.使用程序生成一个页面，同时遍历List集合将用户信息拼接到表格中
        File allUserfile = new File("./showAllUser.html");
        try (
                PrintWriter pw = new PrintWriter(allUserfile,"UTF-8");
                ){
            pw.println("<!DOCTYPE html>");
            pw.println("<html lang=\"en\">");
            pw.println("<head>");
            pw.println("<meta charset=\"UTF-8\">");
            pw.println("<title>用户列表</title>");
            pw.println("</head>");
            pw.println("<body>");
            pw.println("<center>");
            pw.println("<h1>用户列表</h1>");
            pw.println("<table border=\"1\">");
            pw.println("<tr>");
            pw.println("<td>用户名</td>");
            pw.println("<td>密码</td>");
            pw.println("<td>昵称</td>");
            pw.println("<td>年龄</td>");
            pw.println("</tr>");
            for (User user : userList) {
                pw.println("<tr>");
                pw.println("<td>" + user.getUsername() + "</td>");
                pw.println("<td>" + user.getPassword() + "</td>");
                pw.println("<td>" + user.getNickname() + "</td>");
                pw.println("<td>" + user.getAge() + "</td>");
                pw.println("</tr>");
            }
            pw.println("</table>");
            pw.println("</center>");
            pw.println("</body>");
            pw.println("</html>");


        } catch (Exception e) {
            e.printStackTrace();
        }

        //3.将生成的页面设置到响应中发送给浏览器
        File file = new File("./showAllUser.html");
        response.setContentFile(file);

    }


}
