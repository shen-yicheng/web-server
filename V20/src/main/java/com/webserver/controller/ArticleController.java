package com.webserver.controller;

import com.webserver.core.DispatcherServlet;
import com.webserver.entity.Article;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;

public class ArticleController {
    private static File articleDir;
    private static File root;
    private static File staticDir;
    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        articleDir = new File("./articles");
        if (!articleDir.exists()){
            articleDir.mkdirs();
        }
    }


    public void writeArticle(HttpServletRequest request, HttpServletResponse response){

        //1.获取用户在发表上输入的文章信息,获取form表单提交的内容
        String title = request.getParameter("title");
        String message = request.getParameter("message");
        String author = request.getParameter("author");
        if(title==null||message==null||author==null){
            File file = new File(staticDir,"/myweb/wri_input_error.html");
            response.setContentFile(file);
            return;
        }

        System.out.println(title+","+author+","+message);

        //2.将文章信息保存
        File articlFile = new File(articleDir,title+".obj");
        try(
                FileOutputStream fos = new FileOutputStream(articlFile);
                ObjectOutputStream oos = new ObjectOutputStream(fos)
        ){
            Article article = new Article(title,author,message);
            oos.writeObject(article);

            //注册成功了
            File file = new File(staticDir,"/myweb/article_success.html");
            response.setContentFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //3.给用户响应一个注册结果(成功或者失败)

    }
}
