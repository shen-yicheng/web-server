package com.webserver.core;

import com.webserver.annotation.Controller;
import com.webserver.annotation.RequestMapping;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 该类维护所有请求与对应的业务处理类的关系
 */
public class HandlerMapping {

    /*
        key:请求路径

     */
    private static Map<String, MethodMapping> mapping = new HashMap<>();

    static {
        initMapping();
    }


    private static void initMapping() {
        try {
            File dir = new File(
                    DispatcherServlet.class.getClassLoader().getResource(
                            "./com/webserver/controller"
                    ).toURI()
            );
            File[] subs = dir.listFiles(f -> f.getName().endsWith(".class"));
            for (File sub : subs) {
                String fileName = sub.getName();
                String className = fileName.substring(0, fileName.indexOf("."));
                Class cls = Class.forName("com.webserver.controller." + className);
                if (cls.isAnnotationPresent(Controller.class)) {
                    Object controller = cls.newInstance();
                    Method[] methods = cls.getDeclaredMethods();
                    for (Method method : methods) {
                        if (method.isAnnotationPresent(RequestMapping.class)) {
                            String value = method.getAnnotation(RequestMapping.class).value();
                            MethodMapping methodMapping  =new MethodMapping(controller,method);
                            mapping.put(value, methodMapping);
                        }
                    }
                }
            }
        } catch(Exception e){
        e.printStackTrace();
    }
}





    /**
     * 每个实例用于记录一个业务方法以及该方法所属的Controller对象
     */
    public static class MethodMapping{
        private Object controller;
        private Method method;

        public MethodMapping(Object controller, Method method) {
            this.controller = controller;
            this.method = method;
        }

        public Object getController() {
            return controller;
        }

        public Method getMethod() {
            return method;
        }
    }

    /**
     * 根据给定的请求路径获取处理该请求的Controller及对应的业务方法
     * @param path
     * @return
     */
    public static MethodMapping getMethod(String path){
        return mapping.get(path);

    }


    public static void main(String[] args) {
        System.out.println(mapping);
        MethodMapping mm = mapping.get("/myweb/reg");
        System.out.println(mm);

    }
}
