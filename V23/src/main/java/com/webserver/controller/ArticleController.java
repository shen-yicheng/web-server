package com.webserver.controller;

import com.webserver.annotation.Controller;
import com.webserver.annotation.RequestMapping;
import com.webserver.core.DispatcherServlet;
import com.webserver.entity.Article;
import com.webserver.entity.User;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ArticleController {
    private static File articleDir;
    private static File root;
    private static File staticDir;
    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        articleDir = new File("./articles");
        if (!articleDir.exists()){
            articleDir.mkdirs();
        }
    }

    @RequestMapping("/myweb/writeArticle")
    public void writeArticle(HttpServletRequest request, HttpServletResponse response){

        //1.获取用户在发表上输入的文章信息,获取form表单提交的内容
        String title = request.getParameter("title");
        String message = request.getParameter("message");
        String author = request.getParameter("author");
        if(title==null||message==null||author==null){
            File file = new File(staticDir,"/myweb/wri_input_error.html");
            response.setContentFile(file);
            return;
        }

        System.out.println(title+","+author+","+message);

        //2.将文章信息保存
        File articlFile = new File(articleDir,title+".obj");
        try(
                FileOutputStream fos = new FileOutputStream(articlFile);
                ObjectOutputStream oos = new ObjectOutputStream(fos)
        ){
            Article article = new Article(title,author,message);
            oos.writeObject(article);

            //注册成功了
            File file = new File(staticDir,"/myweb/article_success.html");
            response.setContentFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //3.给用户响应一个注册结果(成功或者失败)

    }

    /**
     * 用于显示用户列表的动态页面
     *
     * @param request
     * @param response
     */
    @RequestMapping("/myweb/showAllArticle")
    public void showAllArticle(HttpServletRequest request, HttpServletResponse response) {
        //1 先用users目录里将所有的用户读取出来存入一个List集合备用
        List<Article> articleList = new ArrayList<>();
        /*
            首先获取users中所有名字以.obj结尾的子项 提示:userDir.listFiles()
            然后遍历每一个子项并用文件流连接对象输入流进行反序列化
            最后将反序列化的User对象存入userList集合
         */
        File[] subs = articleDir.listFiles(f -> f.getName().endsWith(".obj"));
        for (File articleFile : subs) {
            try (
                    FileInputStream fis = new FileInputStream(articleFile);
                    ObjectInputStream ois = new ObjectInputStream(fis);
            ) {
                articleList.add((Article) ois.readObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //2 使用程序生成一个页面，同时遍历List集合将用户信息拼接到表格中
        PrintWriter pw = response.getWriter();
        pw.println("<!DOCTYPE html>");
        pw.println("<html lang=\"en\">");
        pw.println("<head>");
        pw.println("<meta charset=\"UTF-8\">");
        pw.println("<title>用户列表</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<center>");
        pw.println("<h1>用户列表</h1>");
        pw.println("<table border=\"1\">");
        pw.println("<tr>");
        pw.println("<td>标题</td>");
        pw.println("<td>作者</td>");
        pw.println("</tr>");
        for (Article article : articleList) {
            pw.println("<tr>");
            pw.println("<td>" + article.getTitle() + "</td>");
            pw.println("<td>" + article.getAuthor() + "</td>");

            pw.println("</tr>");
        }
        pw.println("</table>");
        pw.println("</center>");
        pw.println("</body>");
        pw.println("</html>");

        System.out.println("页面生成完毕!");


        //3 设置响应头Content-Type用于告知动态数据是什么
        response.setContentType("text/html");

    }
}
