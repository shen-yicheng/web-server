package com.webserver.core;

import com.webserver.annotation.Controller;
import com.webserver.annotation.RequestMapping;
import com.webserver.controller.ArticleController;
import com.webserver.controller.ToolsController;
import com.webserver.controller.UserController;
import com.webserver.http.HttpServletRequest;
import com.webserver.http.HttpServletResponse;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.Set;

/**
 * 用于处理请求
 */
public class DispatcherServlet {
    private static File root;
    private static File staticDir;

    static {
        try {
            root = new File(DispatcherServlet.class.getClassLoader().getResource(".").toURI());
            staticDir = new File(root,"static");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void service(HttpServletRequest request, HttpServletResponse response){
        String path = request.getRequestURI();

        System.out.println("========== "+path);

        //首先判断该请求是否为请求一个业务
        /**
         * 1.扫描controller包下所有的业务类，利用反射机制加载，并判断是否被@Controller标注了
         * 2.如果被@Controller标注了，则获取该类中所有本类中定义的方法，并判断是否被@RequestMapping标注了、
         * 3.
         */

        try {
            File dir = new File(
                    DispatcherServlet.class.getClassLoader().getResource(
                            "./com/webserver/controller"
                    ).toURI()
            );
            File[] subs = dir.listFiles(f->f.getName().endsWith(".class"));
            for (File sub:subs){
                String fileName = sub.getName();
                String className = fileName.substring(0,fileName.indexOf("."));
                Class cls = Class.forName("com.webserver.controller."+className);
                if (cls.isAnnotationPresent(Controller.class)){
                    Method[] methods = cls.getDeclaredMethods();
                    for (Method method : methods){
                        if (method.isAnnotationPresent(RequestMapping.class)){
                            String value = method.getAnnotation(RequestMapping.class).value();
                            if (value.equals(path)){
                                //实例化该Controller
                                Object o = cls.newInstance();
                                //执行该方法
                                method.invoke(o,request,response);
                                return;
                            }

                        }
                    }
                }
            }
        } catch (URISyntaxException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        File file = new File(staticDir,path);
        System.out.println("资源是否存在:"+file.exists());

        if(file.isFile()){//当file表示的文件真实存在且是一个文件时返回true
            response.setContentFile(file);
            response.addHeader("Content-Type","text/html");
            response.addHeader("Content-Length",file.length()+"");

        }else{//要么file表示的是一个目录，要么不存在
            response.setStatusCode(404);
            response.setStatusReason("NotFound");
            file = new File(staticDir,"root/404.html");
            response.setContentFile(file);
            response.addHeader("Content-Type","text/html");
            response.addHeader("Content-Length",file.length()+"");
        }






        //测试添加一个额外响应头
        response.addHeader("Server","WebServer");
    }
}






